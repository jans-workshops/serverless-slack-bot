.. PyconCZ 2019 Serverless Slack bot documentation master file, created by
   sphinx-quickstart on Thu Jun  6 09:04:19 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

PyconCZ 2019 Serverless Slack bot
=================================

.. toctree::
   :maxdepth: 3
   :glob:
   :caption: Preparation

   guide/overview
   guide/setup
   guide/slack_setup
   guide/serverless
   guide/slack_msgs


.. toctree::
   :maxdepth: 3
   :glob:
   :caption: Serverless app

   pyapp/python_menu
   pyapp/python_app
   pyapp/python_report
   pyapp/final_deployment

.. toctree::
   :maxdepth: 1
   :glob:
   :caption: How to's & Notes

   help/*