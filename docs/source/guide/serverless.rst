Serverless.js
=============

If you did not install before then install `serverless.js`_ framework:

``npm install -g serverless``


You should have finished basic :ref:`slack setup <guide/slack_setup:Slack setup>` and have your AWS credentials.

#. In your terminal go to the ``sls_app`` directory you created before: ``cd sls_app``.
#. Install python requirements plugin: ``sls plugin install -n serverless-python-requirements``
#. Install AWS pseudo parameters plugin ``sls plugin install -n serverless-pseudo-parameters``
#. Set environment variables in your terminal (in case you do not know how, check :ref:`How to set environment variables <how_to_env_vars>` section:

.. code-block:: text

    AWS_ACCESS_KEY_ID=<your key ID>
    AWS_SECRET_ACCESS_KEY=<your secret key>
    AWS_REGION=eu-west-1
    AWS_DEFAULT_REGION=eu-west-1
    SLACK_TOKEN=<your BOT token>
    SLACK_CHANNEL="<your slack channel ID>"


serverless.yml
--------------

We are going to briefly describe ``serverless.yml`` file, please visit `official page with full description`_.
Most common feature used in our ``serverless.yml`` is dynamic variable replacement (reference).
Example syntax of variables:

.. code-block:: yaml

    yamlKeyXYZ: ${variableSource}
    # this is an example of providing a default value as the second parameter
    otherYamlKey: ${variableSource, defaultValue}

There are many variable sources, please see `the official documentation for more details`_.

This file describes our environment (cloud provider), :ref:`runtime, functions <help/aws_lambda:AWS Lambda>`, plugins,
other cloud resources and the final package.

.. literalinclude:: ../../../sls_app/serverless.yml
   :caption: serverless.yml
   :language: yaml
   :linenos:
   :lineno-match:
   :emphasize-lines: 1,3,11
   :end-at: - "arn:aws:iam::#{AWS::AccountId}:policy/${self:service}-serverless-workshop-policy"

* `service`_ - Provides a namespace for our project
* provider_ - Is the environment where are we going to deploy our application (in our case it's AWS).
  Some attributes can be set at different scopes ``service``, ``provider``, ``function``.
  Everything we set within ``provider`` scope is common for all ``functions``.

* ``provider.iamManagedPolicies`` - `IAM service role`_ with permissions our functions require to run.

    Note: if you are using this lab on your own account, you will need to define your own :ref:`role similar to this one <help/sls_iam_role:Iam Role>`

.. literalinclude:: ../../../sls_app/serverless.yml
   :caption: serverless.yml
   :language: yaml
   :linenos:
   :lineno-match:
   :emphasize-lines: 1
   :start-at: package:
   :end-at: - tests/**

* ``package`` - final ``zip`` file uploaded to S3 and deployed to AWS Lambda

.. literalinclude:: ../../../sls_app/serverless.yml
   :caption: serverless.yml
   :language: yaml
   :linenos:
   :lineno-match:
   :emphasize-lines: 1,3,4,10, 11
   :start-at: functions:
   :end-at: inputTemplate: '{"source": <source>,"time": <eventTime>,"type": "send_report"}'

* ``functions`` - Properties and settings for AWS Lambda functions.
* ``functions.<fn-name>.handler`` - Path to python module containing :ref:`lambda_handler function <help/aws_lambda:Handler>`
* ``functions.<fn-name>.events`` - AWS Lambda function triggers

.. literalinclude:: ../../../sls_app/serverless.yml
   :caption: serverless.yml
   :language: yaml
   :linenos:
   :lineno-match:
   :emphasize-lines: 1
   :start-at: plugins:
   :end-at:  - serverless-wsgi

* ``plugins`` - Serverless.js plugins (some are installed automatically, others must be installed with ``sls plugin install -n <plugin-name>``

.. literalinclude:: ../../../sls_app/serverless.yml
   :caption: serverless.yml
   :language: yaml
   :linenos:
   :lineno-match:
   :emphasize-lines: 1
   :start-at: custom:
   :end-at: - "**/*.dist-info*"

* ``custom`` - Custom variables, which can be referenced as ``${self:custom.<variableName>}``

.. literalinclude:: ../../../sls_app/serverless.yml
   :caption: serverless.yml
   :language: yaml
   :linenos:
   :lineno-match:
   :emphasize-lines: 1
   :start-at: resources:

* ``resources`` - definition of additional AWS CloudFormation resources (`serverless.js docs`_, `aws CloudFormation docs`_)

.. _serverless.js: https://serverless.com
.. _official page with full description: https://serverless.com/framework/docs/providers/aws/guide/serverless.yml/
.. _service: https://serverless.com/framework/docs/providers/aws/guide/services/
.. _provider: https://serverless.com/framework/docs/providers/
.. _the official documentation for more details: https://serverless.com/framework/docs/providers/aws/guide/variables/
.. _IAM service role: https://docs.aws.amazon.com/IAM/latest/UserGuide/id_roles_terms-and-concepts.html
.. _serverless.js docs: https://serverless.com/framework/docs/providers/aws/guide/resources/
.. _aws CloudFormation docs: https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/Welcome.html