Slack setup
===========

We will be using Slack web client via your favorite web browser.

#. Join our Slack workspace ``slspyconcz2019`` or `invite yourself`_
#. Create a channel ``iam_username-team``
#. Make a note with your ``CHANNEL_ID`` from your channel's url ``https://slspyconcz2019.slack.com/messages/<CHANNEL_ID>/``


Slack app
~~~~~~~~~

#. Go to https://api.slack.com/apps
#. Click ``Create New App``
#. Fill in appName (to keep it simple: ``iam_username-bot`` and select workspace ``slspyconcz2019`` workspace
#. Click Features:``Bot Users`` -> ``Add a Bot User``, keep default values -> click green ``Add a Bot User``
#. Go to ``oAuth & Permissions`` -> ``Install app to Workspace`` -> ``Authorize`` (this generates Bot Token)

Keep the ``Bot Token`` handy as you will need it later.

Now basic slack setup is finished, continue to set up the serverless framework

.. _invite yourself: https://join.slack.com/t/slspyconcz2019/shared_invite/enQtNjYzNTQwNjgzMjM2LWYwZWQxZWVmNWJmZDk1ZDEyZjg1Y2M1NGQyZDZlOGRlMDNiNGY4YWRlYjRiOWJiZjMwMmU0YzE4YmM0ODFhYTI