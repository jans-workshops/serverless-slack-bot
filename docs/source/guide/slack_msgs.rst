Slack messages
==============

Before we start coding let's find out how Slack is composing the message.
You can always refer to `official documentation`_.

We are only going to cover parts we will use through out the guide, plase refer to `full message documentation`_ if needed.

Slack message is a simple JSON object.

.. code-block:: json

    {
        "text": "A message text",
        "blocks": [],
        "mrkdwn": true
    }


.. csv-table::
   :file: ../_files/table.csv
   :header-rows: 1
   :class: longtable

We are going to use a new Slack feature `blocks`_ to compose our messages.

Our main block components will be `Section Block`_ which is similar to the message.
And `Action blocks`_ is used for interactive components like buttons and menus.
Most important block attributes to understand are: ``type``, ``block_id``, ``fields`` and ``elements``.
Please go to the documentation and try it out using a `block-kit-builder`_.

.. Once you are comfortable about layout blocks go ahead to the next section: :ref:`Sending daily menu lambda function <guide/python_menu:Daily menu>`

There are other Slack objects we are going to deal with

.. _official documentation: https://api.slack.com/messaging/managing#message
.. _full message documentation: https://api.slack.com/reference/messaging/payload
.. _block-kit-builder: https://api.slack.com/tools/block-kit-builder
.. _blocks: https://api.slack.com/reference/messaging/blocks
.. _Action blocks: https://api.slack.com/reference/messaging/blocks#actions
.. _Section Block: https://api.slack.com/reference/messaging/blocks#section