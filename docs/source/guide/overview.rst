Project overview
================

We are going to build a serverless Slack application to simulate daily standup meeting.

How is it going to work?

#. Application will be installed to our team's ``SLACK_CHANNEL``
#. At time event (eg 8:30) (Time to ask questions) - application will send Direct Messages to the users and collect the responses.
#. At time event (eg. 10:00) (Time to send report) - application collects data from the database and send it back to the ``SLACK_CHANNEL``

.. raw:: html
    :file: schema.svg


You can also `look at the schema via draw.io`_

Users will interact with our app via Slack.

#. User receives a menu into direct message (private message)

    .. image:: ../_files/menu.png
      :alt: menu
      :scale: 50%

#. Click on ``Open Dialog`` button opens a dialog with questions

    .. image:: ../_files/dialog.png
      :alt: dialog
      :scale: 50%

#. Application notifies user when dialog is successfully sent

    .. image:: ../_files/menu_response.png
      :alt: dialog
      :scale: 50%

#. Send a report after a given time

    .. image:: ../_files/report.png
      :alt: report
      :scale: 50%

.. _look at the schema via draw.io: https://drive.google.com/file/d/10-liPD58fXncBFwLysIvQvj-cj05S9xK/view?usp=sharing