Basic setup
===========

First we need to prepare our environment.

You will need:

* A terminal program (Powershell, xterm, etc...) and a directory (``sls_workshop``).
* Installed `python3.6`_ or newer, nodejs_ with npm_.
* AWS IAM user name (PyconCZ 2019 will receive this before the workshop)

This tutorial will make the best effort to support ``powershell`` on windows and any ``terminal`` with ``bash`` on Linux/Mac.
If you are using ``git bash`` on windows you should be able to follow steps for Linux/Mac

#. Open your terminal
#. Create a project directory: ``mkdir sls_workshop``
#. Change your current working directory to one we just created: ``cd sls_workshop``
#. Create a virtual environment ``python -m venv slsenv``
#. Activate virtual environment

   * Linux/Mac: ``. slsenv/bin/activate``
   * Windows: ``. .\slsenv\Scripts\activate.ps1``

#. Install following tools with ``pip``: ``pip install cookiecutter awscli``
#. Use cookiecutter to get server less app template ``cookiecutter https://gitlab.com/jans-workshops/pyconcz-2019-slack-bot-template.git``
#. Enter your AWS IAM user name and press enter.

Cookie cutter will create a new directory with our serverless app.

.. code-block:: text

    sls_app/
    ├── example-data
    │   ├── apigw-block_action.json
    │   ├── block_action.json
    │   ├── cwe-cron.json
    │   ├── cwe-questions.json
    │   ├── cwe-report.json
    │   ├── dialog_submission.json
    │   ├── sample_dialog.json
    │   └── verification.json
    ├── requirements.txt
    ├── serverless.yml
    └── standup_bot
        ├── __init__.py
        ├── action_app.py
        ├── config.py
        ├── models.py
        ├── msg_templates.py
        └── scheduled.py

* ``serverless.yml`` - configuration file for serverless.js framework, which describes our deployment
* ``example-data/`` - directory contains examples of data structures we deal with -
  good reference once you are comfortable with theory behind it.
* ``standup_bot`` - python module representing our application
* ``standup_bot/action_app.py`` - Flask application processing responses from Slack deployed as ``slack-responses`` AWS Lambda function
* ``standup_bot/config.py`` - contains some configuration used by both lambda functions
* ``standup_bot/models.py`` - contains our database models
* ``standup_bot/msg_templates.py`` - contains slack message `blocks`_ and templates (JSON objects)
* ``standup_bot/scheduled.py`` - contains logic for scheduled events to send menu or report


Now you are ready to start building the Serverless Slack bot.

Your next step is to set up a :ref:`Slack <guide/slack_setup:Slack setup>`

.. _nodejs: https://nodejs.org/
.. _npm: https://www.npmjs.com
.. _python3.6: https://www.python.org/downloads/
.. _blocks: https://api.slack.com/block-kit