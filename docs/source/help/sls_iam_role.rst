Iam Role
========

Workshop apps hosted inside our environment are protected via managed policy.
If you are going through this in your own environment, please use following service role for lambdas.

.. literalinclude:: ../_files/sls_iam_role.yaml
   :language: yaml
   :linenos:
   :lineno-match:
   :emphasize-lines: 3


Remove the section ``provider.iamManagedPolicies``
from our :ref:`original serverless.yaml <guide/serverless:serverless.yml>`
and insert ``iamRoleStatements`` section from above.