AWS DynamoDB
============

DynamoDB is a no relational database in AWS. This will only cover the part
required for the workshop but feel free to refer to the `official documentation`_.

In this workshop, we need a way to store our state, this mean we need the questions and
answers of our users in a database so we can publish the results to the chat when
the time comes.

To do so, at ``serverless.yml`` we create a table with two items:


.. literalinclude:: ../../../sls_app/serverless.yml
   :caption: serverless.yml
   :language: yaml
   :linenos:
   :lineno-match:
   :emphasize-lines: 1,3,11
   :start-at: StandupDynamoDbTable:

- report_id: incremental ID of every report
- report_user_id: the user ID that submitted the dialog

After submitting the questions, this table will be updated with:

- answers: dictionary with every question number as a key and the answer as value

When the standup time comes, ``scheduled.py`` will trigger ``all_reports`` functions.

.. _official documentation: https://aws.amazon.com/dynamodb/getting-started/
