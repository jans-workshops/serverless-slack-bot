Amazon Web Services
===================

These locations are composed of Regions and Availability Zones.
Each Region is a separate geographic area. Each Region has multiple,
isolated locations known as Availability Zones.

.. image:: https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/images/aws_regions.png


Underlying AWS services
=======================

* :ref:`IAM <help/aws_iam:IAM>` - AWS Identity and Access Management (IAM), With IAM, you can centrally manage users,
  security credentials such as access keys, and permissions that control which AWS resources users and applications can access.

* `S3 (Simple Storage Service)`_ -  is storage for the internet.
   Designed for durability of 99.999999999% of objects across multiple Availability Zones

* :ref:`Lambda <help/aws_lambda:AWS Lambda>` - Function as a service

* `Cloud Watch`_ - Amazon CloudWatch provides a reliable, scalable, and flexible monitoring solution

* `API Gateway`_ - Amazon API Gateway enables you to create and deploy your own REST and WebSocket APIs at any scale.

* :ref:`DynamoDB <help/aws_dynamodb:AWS DynamoDB>` - Amazon DynamoDB is a fully managed NoSQL database

* `CloudFormation`_ - AWS Infrastructure as a Code

.. _S3 (Simple Storage Service): https://docs.aws.amazon.com/s3/index.html
.. _Cloud Watch: https://docs.aws.amazon.com/cloudwatch/index.html
.. _API Gateway: https://docs.aws.amazon.com/apigateway/index.html
.. _CloudFormation: https://docs.aws.amazon.com/cloudformation/index.html
