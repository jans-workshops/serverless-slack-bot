.. _how_to_env_vars:

How to set environment variables
================================

Mac/Linux (Bash)
----------------

.. code-block:: bash

    export AWS_ACCESS_KEY_ID=<your key ID>
    export AWS_SECRET_ACCESS_KEY=<your secret key>
    export AWS_REGION=us-east-1
    export AWS_DEFAULT_REGION=us-east-1
    export SLACK_TOKEN=<your BOT token>
    export SLACK_CHANNEL="<your slack channel ID>"

Windows
-------

Powershell

.. code-block:: powershell

    $env:AWS_ACCESS_KEY_ID = "<your key ID>"
    $env:AWS_SECRET_ACCESS_KEY = "<your secret key>"
    $env:AWS_REGION = "us-east-1"
    $env:AWS_DEFAULT_REGION = "us-east-1"
    $env:SLACK_TOKEN = "<your BOT token>"
    $env:SLACK_CHANNEL = "<your slack channel ID>"

Git-bash

.. code-block:: bash

    export AWS_ACCESS_KEY_ID=<your key ID>
    export AWS_SECRET_ACCESS_KEY=<your secret key>
    export AWS_REGION=us-east-1
    export AWS_DEFAULT_REGION=us-east-1
    export SLACK_TOKEN=<your BOT token>
    export SLACK_CHANNEL="<your slack channel ID>"

CMD (not recommended)

.. code-block:: bat

    setx AWS_ACCESS_KEY_ID "<your key ID>"
    setx AWS_SECRET_ACCESS_KEY "<your secret key>"
    setx AWS_REGION "us-east-1"
    setx AWS_DEFAULT_REGION "us-east-1"
    setx SLACK_TOKEN "<your BOT token>"
    setx SLACK_CHANNEL "<your slack channel ID>"

