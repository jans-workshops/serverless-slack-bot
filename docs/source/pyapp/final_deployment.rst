Final deployment
================

To deploy our app we need to go through few steps:

#. ``cd sls_app`` (you should be already there)
#. ``sls deploy`` - this will deploy our app to AWS and output API Gateway endpoint

   .. image:: ../_files/endpoint.png
      :alt: endpoint
      :scale: 50%

#. Enable Slack Interactive Components


Enabling Slack Interactive Components
-------------------------------------

#. Navigate to https://api.slack.com/apps
#. Click on your application
#. Features -> **Interactive Components**
#. Set interactive components to On

   .. image:: ../_files/interactive_off.png
      :alt: interactive button
      :scale: 50%

#. Fill in the endpoint URL

   .. image:: ../_files/interactive_endpoint.png
      :alt: interactive endpoint
      :scale: 50%

#. Hit the ``Save Changes`` button


Local invocation
----------------

Because our Cloud Watch events are disabled to prevent unwanted executions before the production/test stage.


.. literalinclude:: ../../../sls_app/serverless.yml
   :language: yaml
   :linenos:
   :lineno-match:
   :emphasize-lines: 8, 18
   :start-at: scheduled-events:
   :end-at: inputTemplate: '{"source": <source>,"time": <eventTime>,"type": "send_report"}'

You may have noticed that ``rate`` is set to `rate which is CloudWatch scheduled event expression`_
and this is also not correct for production, however it's a good way to observe behaviour triggered by AWS.

For now we invoke our scheduled functions locally and manually via serverless framework commands.

To ``send_questions`` run

.. code-block:: text

    sls invoke local -f scheduled-events --path example-data/cwe-questions.json

Then take some time to collect the data and once you are ready.
Trigger ``send_report`` using the command

.. code-block:: text

    sls invoke local -f scheduled-events --path example-data/cwe-report.json

.. _rate which is CloudWatch scheduled event expression: https://docs.aws.amazon.com/AmazonCloudWatch/latest/events/ScheduledEvents.html