Serverless Slack application
============================

This repository is a template for a serverless Slack application.
In order to build this application you need access to AWS cloud resources.

Head on to https://pycon-sls-2019.readthedocs.io/en/latest/ for detailed walk through.

AWS Resources & Services
------------------------

Here is a list of AWS services we are going to use during the tutorial.

* CloudFormation
* API Gateway
* CloudWatch Logs and Events
* Lambda
* DynamoDB
* IAM
* S3 bucket


Slack
~~~~~

You will also need a Slack workspace.
Attendees of PyconCZ 2019 can use our workspace.

We will be using Slack web client via your favorite web browser.

#. Join our Slack workspace ``slspyconcz2019`` or `invite yourself`_ (link is active for 30 days)
#. Create a channel ``iam_username-team``
#. Make a note with your ``CHANNEL_ID`` from your channel's url ``https://slspyconcz2019.slack.com/messages/<CHANNEL_ID>/``

.. _invite yourself: https://join.slack.com/t/slspyconcz2019/shared_invite/enQtNjYzNTQwNjgzMjM2LWYwZWQxZWVmNWJmZDk1ZDEyZjg1Y2M1NGQyZDZlOGRlMDNiNGY4YWRlYjRiOWJiZjMwMmU0YzE4YmM0ODFhYTI