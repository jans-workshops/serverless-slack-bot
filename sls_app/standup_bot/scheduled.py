#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Standalone lambda function triggered by CWE.
"""
import datetime as dt
import json
import logging

from slack import WebClient

from standup_bot.config import (
    SLACK_TOKEN,
    SLACK_CHANNEL,
    QUESTIONS,
)
from standup_bot.models import Report
from standup_bot.msg_templates import standup_menu_block, report_block

LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.INFO)

# synchronous slack client
SC = WebClient(SLACK_TOKEN)


# ##### questions ###########
def send_menu(user_id, menu_block):
    """Send menu as private message to the user."""

    response = SC.conversations_open(users=[user_id])
    post_response = SC.chat_postMessage(
        channel=response["channel"]["id"], text="Daily menu", blocks=menu_block
    ),
    return user_id, post_response


def send_menus(menu_block):
    """Send menu to all users from the channel."""
    members = SC.conversations_members(channel=SLACK_CHANNEL)
    for user_id in members['members']:
        yield send_menu(user_id, menu_block)


def send_questions(report_id):
    """Entry point for daily menu."""
    menu_block = standup_menu_block(report_id)
    results = list(send_menus(menu_block))
    LOGGER.info(results)


# ######## report ##############
def one_report(report):
    """Show report of one user."""
    user_id = report.user_id

    response = SC.chat_postMessage(
        channel=SLACK_CHANNEL,
        username=report.display_name,
        icon_url=report.icon_url,
        text=f"*{report.display_name}* posted and update for stand up meeting.",
        blocks=report_block(QUESTIONS, report.answers)
    )

    return user_id, response


def all_reports(report_id):
    """Show reports of all users."""
    reports = Report.query(report_id, Report.report_user_id.startswith(report_id))

    SC.chat_postMessage(
        channel=SLACK_CHANNEL,
        text=f"Here are the standup results from *{dt.datetime.strptime(report_id, '%Y%m%d')}*.",
    )
    for report in reports:
        yield one_report(report)


def send_report(report_id):
    """Entry point for sending reports."""
    results = list(all_reports(report_id))
    LOGGER.info("Sent report log: %s", results)


# ############ main handler #####################

def lambda_handler(lambda_event, lambda_context):
    """Main lambda handler"""
    LOGGER.debug(lambda_context)
    LOGGER.info(lambda_event)
    LOGGER.info("lambda_event starts:")
    LOGGER.info(json.dumps(lambda_event))

    # is it our CWE event?
    if {"type", "time", "source"}.issubset(lambda_event):

        report_id = dt.datetime.strptime(lambda_event["time"], '%Y-%m-%dT%H:%M:%S%z').strftime("%Y%m%d")
        LOGGER.info("Report ID: %s", report_id)
        if lambda_event["type"] == "send_report":
            send_report(report_id)
        elif lambda_event["type"] == "send_questions":
            send_questions(report_id)
