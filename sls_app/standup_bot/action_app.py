"""
Main flask app file used to receive incoming http requests from Slack.
"""
import logging
from urllib import parse

from flask import Flask, request, json, make_response
from slack import WebClient

from standup_bot.config import QUESTIONS, SLACK_TOKEN
from standup_bot.models import Report
from standup_bot.msg_templates import dialog_questions

# synchronous slack client
SC = WebClient(SLACK_TOKEN)

app = Flask(__name__)
app.config["SECRET_KEY"] = "you-will-never-guess"
app.logger.setLevel(logging.INFO)


@app.route("/actions", methods=["POST"])
def actions():
    """
    Endpoint /actions to process actions and dialogs.

    This method receives a request from API gateway.
    Example request: example-data/apigw-block_action.json

    We need to decode body and decide if requests is one of:
    - block_actions -> will trigger process_block_actions()
    - dialog_submission -> will trigger process_dialogs()

    Returns
    -------
    flask.Response

    """

    prefix = "payload="
    data = request.get_data(as_text=True)[len(prefix):]
    app.logger.info("req_data %s", data)
    # app.logger.info("dd %s", data[len(prefix):])

    # action body
    slack_req_body = json.loads(parse.unquote_plus(data))
    app.logger.info("Action body: %s", slack_req_body)

    slack_req_type = slack_req_body.get("type")

    action = {"block_actions": process_block_actions, "dialog_submission": process_dialogs}

    response = action[slack_req_type](slack_req_body)
    app.logger.info("Response to Action: %s : %s", response, response.get_data())
    return response


def process_block_actions(slack_request: dict):
    """
    Slack Action processor.

    Here we are going to process decoded slack request "block actions"
    https://api.slack.com/reference/messaging/blocks#actions

    Example request: example-data/block_action.json

    We will present user with 2 buttons.
    1. Open dialog - which contains standup questions
    2. Skip today - to let user pass the meeting

    Returns
    -------
    flask.Response
        Empty response 200 signifies success.

    """
    action = slack_request["actions"][0]
    state_data = {"container": slack_request["container"], "report_id": action["value"]}
    if action["action_id"] == "standup.action.open_dialog":
        questions = dialog_questions(json.dumps(state_data), QUESTIONS)

        app.logger.info(questions)

        slack_response = SC.dialog_open(
            dialog=questions, trigger_id=slack_request["trigger_id"]
        )
        app.logger.info("Dialog Open: %s", slack_response)
        return make_response()

    if action["action_id"] == "standup.action.skip_today":
        # you can try to implement this yourself
        pass

    return make_response("Unable to process action", 400)


def process_dialogs(slack_dialog: dict):
    """
    Process Slack dialogs.

    Here we are going to collect data from dialog

    example dialog submission: example-data/dialog_submission.json

    Returns
    -------
    flask.Response
        Successful dialog submission requires empty response 200.

    """

    if slack_dialog["callback_id"] == "standup.action.answers":
        # add one field to answers
        state_data = json.loads(slack_dialog["state"])
        # prepare DB record

        # get more user data
        user_info = SC.users_info(
            user=slack_dialog["user"]["id"]
        )

        app.logger.info("UserInfo: %s", user_info)

        display_name = (
                user_info["user"]["profile"]["display_name"]
                or user_info["user"]["profile"]["real_name"]
                or slack_dialog["user"]["name"]
        )

        user_report = Report(
            state_data["report_id"],
            f'{state_data["report_id"]}_{slack_dialog["user"]["id"]}',
            user_name=slack_dialog["user"]["name"],
            user_id=slack_dialog["user"]["id"],
            answers=slack_dialog["submission"],
            display_name=display_name,
            icon_url=user_info["user"]["profile"]["image_48"],
        )
        # Write to database.
        user_report.save()
        # Respond to user
        SC.chat_update(
            channel=state_data["container"]["channel_id"],
            ts=state_data["container"]["message_ts"],
            text="Thank you for your submission.",
            blocks=[],
            as_user=True,  # reason specified in slack docs
        )

        app.logger.info("Adding new answer: %s", user_report._get_json())
        return make_response()

    return make_response("Unable to process dialog", 400)
