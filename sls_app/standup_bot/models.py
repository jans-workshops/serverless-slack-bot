"""Dynamo db models."""

from pynamodb.attributes import MapAttribute, UnicodeAttribute
from pynamodb.models import Model

from standup_bot.config import TABLE_NAME, AWS_REGION


class Report(Model):
    """
    Standup report model.
    """

    class Meta:
        table_name = TABLE_NAME
        region = AWS_REGION

    report_id = UnicodeAttribute(hash_key=True)
    report_user_id = UnicodeAttribute(range_key=True)
    user_id = UnicodeAttribute()
    user_name = UnicodeAttribute()
    display_name = UnicodeAttribute()
    icon_url = UnicodeAttribute()
    answers = MapAttribute()
