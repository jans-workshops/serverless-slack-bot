"""Portions of messages sent to the Slack."""


def dialog_questions(state, questions):
    """
    Create Slack dialog with questions.

    Here we are going to create a dialog opened via button click.
    Result dialog will look like: example-data/sample_dialog.json

    """
    dialog = {
        "callback_id": "standup.action.answers",
        "title": "Daily standup questions.",
        "elements": [],
        "state": state,
    }

    for idx, question in questions.items():
        dialog["elements"].append(
            {
                "label": question,
                "name": idx,
                "type": "textarea",
                "hint": question,
                "placeholder": question,
                "optional": True,
            }
        )

    return dialog


def standup_menu_block(report_id):
    """
    Message block with the menu sent on daily basis.

    Contains Open Dialog and Skip buttons.

    """
    return [
        {
            "type": "section",
            "text": {
                "type": "mrkdwn",
                "text": "Hello, it is time report on daily standup.",
            },
        },
        {
            "type": "actions",
            "elements": [
                {
                    "type": "button",
                    "text": {
                        "type": "plain_text",
                        "emoji": True,
                        "text": "Open Report",
                    },
                    "style": "primary",
                    "value": report_id,
                    "action_id": "standup.action.open_dialog",
                },
                {
                    "type": "button",
                    "text": {"type": "plain_text", "emoji": True, "text": "Skip today"},
                    "action_id": "standup.action.skip_today",
                },
            ],
        },
    ]


def report_block(questions, answers):
    """Message block used for reports."""
    block = []
    for question_id, question in questions.items():
        if answers[question_id]:
            qa_block = {
                "type": "section",
                "text": {
                    "type": "mrkdwn",
                    "text": f"*{question}*\n" f"{answers[question_id]}",
                },
            }
            block.append(qa_block)

    return block
